# auto_analysisRun38.R
# Paul Aiyetan
# 10/10/2016


# this analysis run computes the concordance index for the rscores derived by each of the
#    i) logit.rscores
#   ii) logit.rscores.weighted
#  iii) coxph.rscores, and
#   iv) coxph.rscores.weighted
#      for each of the top performing feature-selection algorithm pipelines, on the 'expMatLRtWithBiClassInfo'


rm(list=ls())
load(file="./objs/expMatLRtWithBiClassInfo.logit.rscores.rda")
load(file="./objs/expMatLRtWithBiClassInfo.logit.rscores.weighted.rda")
load(file="./objs/expMatLRtWithBiClassInfo.coxph.rscores.rda")
load(file="./objs/expMatLRtWithBiClassInfo.coxph.rscores.weighted.rda")

load("./objs/expMatLRtWithBiClassInfo.rda")
load("./objs/allMergedNoDupsDatasetsPDataTable.rda")

require(genefu)
require(rmeta)
source("./codes/computeConcordanceIndeces.R")



rsTablesList <- list(logit = expMatLRtWithBiClassInfo.logit.rscores,
                     logit.w = expMatLRtWithBiClassInfo.logit.rscores.weighted,
                     coxph = expMatLRtWithBiClassInfo.coxph.rscores,
                     coxph.w = expMatLRtWithBiClassInfo.coxph.rscores.weighted)

# compute c.index
expMatLRtWithBiClassInfo.cIndeces <- computeConcordanceIndeces(rscoresTablesList = rsTablesList,
                                                   expMatWithClassInfo = expMatLRtWithBiClassInfo,
                                                      phenoTable = allMergedNoDupsDatasetsPDataTable,
                                                         pheno.attr = "lr.time")
# save
save(expMatLRtWithBiClassInfo.cIndeces, file="./objs/expMatLRtWithBiClassInfo.cIndeces.rda")
load(file="./objs/expMatLRtWithBiClassInfo.cIndeces.rda")


# write
write.table(as.data.frame(do.call("rbind",expMatLRtWithBiClassInfo.cIndeces),stringsAsFactors=FALSE),
            file="./texts/expMatLRtWithBiClassInfo.cIndeces.tsv", sep="\t",quote=FALSE)

# plot
png(filename="./figs/expMatLRtWithBiClassInfo.cIndeces.tData.png",
        height=3000, width=3000, res=350)
par(mfcol=c(2,2))
for(i in 1:length(expMatLRtWithBiClassInfo.cIndeces)){
    mtdId <- names(expMatLRtWithBiClassInfo.cIndeces)[i]
    #png(filename=paste("./figs/expMatLRtWithBiClassInfo.cIndeces.",
    #                   mtdId,".tData.png",sep="",collapse=""),
    #    height=3500, width=3500, res=350)
    cI <- expMatLRtWithBiClassInfo.cIndeces[[mtdId]]
    metaplot.surv(mn=cI$cindex,lower=cI$lower,upper=cI$upper,labels=rownames(cI),xlim=c(0.3,0.9),
             boxsize=0.5, zero=0.5,col=meta.colors(box="royalblue",line="darkblue",zero="firebrick"),
             main=mtdId,cex.main=0.95)
    #dev.off()
}
dev.off()
