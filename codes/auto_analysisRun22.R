# auto_analysisRun22.R
# Paul Aiyetan
# September 13, 2016


#  recursive pass top 30 - 170 features to discovery pipelines passing threshold in previous
#    scripts and observe identifications..
rm(list=ls())
require(rpart)
require(e1071)
require(FSelector)
require(parallel)

source("./codes/runFeatureSelection.R")

# load ranked features
load("./objs/expMatMRtWithBiClassInfo.SelectedFeatures.fcbf.rda")
load("./objs/expMatMRtWithBiClassInfo.RankedFeatures.cor.rda")
load("./objs/expMatMRtWithBiClassInfo.RankedFeatures.ig.rda")
load("./objs/expMatMRtWithBiClassInfo.RankedFeatures.gr.rda")
load("./objs/expMatMRtWithBiClassInfo.RankedFeatures.su.rda")
# load("./objs/expMatMRtWithBiClassInfo.RankedFeatures.chi2.rda") # didn't pass threshold from previous

load("./objs/expMatMRtWithBiClassInfo.disc.CAIM.rda")

rankedFeaturesTablesList1 <-
    list(fcbf = expMatMRtWithBiClassInfo.SelectedFeatures.fcbf,
         cor = expMatMRtWithBiClassInfo.RankedFeatures.cor,
         ig = expMatMRtWithBiClassInfo.RankedFeatures.ig,
         gr = expMatMRtWithBiClassInfo.RankedFeatures.gr,
         su = expMatMRtWithBiClassInfo.RankedFeatures.su#,
         #chi2 = expMatMRtWithBiClassInfo.RankedFeatures.chi2 # didn't pass threshold from previous analysis
         )


# ---- feature selection for respective topFeatures techniques ---- #
topFeaturesNumbers <- seq(20,180,10)
filterMethods <- c("fcbf","cor",
                   #"chi2", # did not pass desired threshold from previous analysis
                   "su","ig","gr")
eval.algorithms <- c("rpart",
                     "nb",
                     "svm")

cl <- makeCluster(getOption("cl.cores",10))
clusterExport(cl = cl, varlist = c("filterMethods","eval.algorithms",
                                   "rankedFeaturesTablesList1",
                                   "expMatMRtWithBiClassInfo.disc.CAIM"),
              envir = .GlobalEnv)
topFeaturesFeatureSelectionResultsList <-
    parLapply(cl,topFeaturesNumbers,
                          function(x){

        source("./codes/runFeatureSelection.R")
        require("FSelector")
        require("rpart")
        require("e1070")
                              
        topFeatures <- x
        #pb <- txtProgressBar(min=0,max=length(filterMethods),style=3)   
        filterMethodsFeatureSelectionResultsList <- list()
        for(i in 1:length(filterMethods)){
            filterMethodName <- filterMethods[i]
            noOfFeatures <- nrow(rankedFeaturesTablesList1[[filterMethodName]])
            if ( noOfFeatures < topFeatures ){
                                        #topFeatures <- noOfFeatures
                filterMethodsFeatureSelectionResult <- NULL
            
            } else {
                                        # parallelize... to speed up process.
                featuresTablesL <- rankedFeaturesTablesList1[filterMethodName] # a list of one element
                evalAlgorithmsFeatureSelectionResults <- list()
                for(j in 1:length(eval.algorithms)){
                    eval.al <- eval.algorithms[j]
                    topF <- topFeatures
                    dataList <- list(rankedFeaturesTablesList=featuresTablesL,#featuresTablesList,
                                     expMatWithClassInfo=expMatMRtWithBiClassInfo.disc.CAIM)
                    evalAlgorithmFeaturesResults <-
                        runFeatureSelection(dataList,
                                            type="supervised",kFold=10,
                                            seed=1,
                                            eval.algorithm=eval.al,#eval.alg,
                                            topFeatures=topF,
                                            search.algorithms="bs")#topFeat)
                    evalAlgorithmsFeatureSelectionResults[[eval.al]] <- evalAlgorithmFeaturesResults
                }

                filterMethodsFeatureSelectionResult <- evalAlgorithmsFeatureSelectionResults
            }
            filterMethodsFeatureSelectionResultsList[[filterMethodName]] <-
                filterMethodsFeatureSelectionResult
        }
        return(filterMethodsFeatureSelectionResultsList)

    })
stopCluster(cl)
names(topFeaturesFeatureSelectionResultsList) <- as.character(topFeaturesNumbers)
expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList <-
    topFeaturesFeatureSelectionResultsList

save(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
     file="./objs/expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList.rda")

  
# extractTopFeatsSpecificResultsTable
source("./codes/extractTopFeatsSpecificResultsTable.R")

# get accuracy table...
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="acc")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc.rda")

# get min.acc table...
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="min.acc")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc.rda")


# get max.acc table..
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="max.acc")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc.rda")

# get sd.acc table..
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="sd.acc")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc.rda")



# get error.rate table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="err.rate")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableErr.rda")


# get min.err.rate table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="min.err.rate")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr.rda")


# get max.err.rate table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxErr <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="max.err.rate")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxErr,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxErr.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinErr,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxErr.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxErr.rda")

# get sd.err.rate table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="sd.err.rate")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDErr.rda")



# get noOfFeats selected table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTable <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="noOfFeats")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTable.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTable.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTable.rda")

# get Features selected table
expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures <- 
    extractTopFeatsSpecificResultsTable(expMatMRtWithBiClassInfo.topFeaturesFeatureSelectionResultsList,
                                        what="featsSelected")
write.table(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures,
            file="./texts/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures.tsv",sep="\t",
            quote=FALSE,row.names=TRUE)
save(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures,
     file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures.rda")
load(file="./objs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableFeatures.rda")



# plot accuracies...
png(filename="./figs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc.png",
    height=1500,width=1500,res=350)
chartsToPlot <- c("fcbf.svm.BSResults",
                  "cor.rpart.BSResults",
                  "cor.svm.BSResults",
                  "su.rpart.BSResults",
                  "ig.rpart.BSResults",
                  "ig.svm.BSResults",
                  "gr.svm.BSResults")                  
plot(seq(20,180,10),
     expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     type="l",las=1,ylab="Accuracy",xlab="Features", col=1, ylim=c(0.76,0.81))
for(i in 2:length(chartsToPlot)){
    lines(seq(20,180,10),
          expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[i],], type="l", col=i)
}
legend(x="topright",legend=chartsToPlot,col=c(1:length(chartsToPlot)),cex=0.35, lty = "solid")
grid(nx=10,lty="solid",lwd=0.25)
dev.off()

# plot accuracies (narrow down)...
png(filename="./figs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc2.png",
    height=1500,width=1500,res=350)
chartsToPlot <- c("fcbf.svm.BSResults",
                  #"cor.rpart.BSResults",
                  #"cor.svm.BSResults",
                  "su.rpart.BSResults",
                  #"ig.rpart.BSResults",
                  "ig.svm.BSResults"#,
                  #"gr.svm.BSResults"
                  )                  
plot(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     type="l",las=1,ylab="Accuracy",xlab="Features", col=1, ylim=c(0.76,0.81))
lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],], type="l", col=4)
lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],], type="l", col=6)
legend(x="topright",legend=chartsToPlot,col=c(1,4,6),cex=0.35, lty = "solid")
grid(nx=10,lty="solid",lwd=0.25)
dev.off()


# plot accuracies (narrow down and add texts)...
png(filename="./figs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc3.png",
    height=1500,width=1500,res=350)
chartsToPlot <- c("fcbf.svm.BSResults",
                  #"cor.rpart.BSResults",
                  #"cor.svm.BSResults",
                  "su.rpart.BSResults",
                  #"ig.rpart.BSResults",
                  "ig.svm.BSResults"#,
                  #"gr.svm.BSResults"
                  )                  
plot(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     type="l",las=1,ylab="Accuracy",xlab="Features", col=1, ylim=c(0.76,0.81))
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     gsub("0","",as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[1],])),
     col=1,cex=0.4,pos=1)
                                                                         
lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],], type="l", col=4)
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[2],]),
     col=4,cex=0.4,pos=1)

lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],], type="l", col=6)
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[3],]),
     col=6,cex=0.4,pos=1)


legend(x="topright",legend=chartsToPlot,col=c(1,4,6),cex=0.35, lty = "solid")
#grid(nx=10,lty="solid",lwd=0.25)
abline(h=c(seq(0.76,0.81,by=0.00125)),v=c(seq(20,180,2.5)),col="lightgray",lty="solid",lwd=0.125)
abline(h=c(seq(0.76,0.81,by=0.005)),v=c(seq(20,180,10)),col="gray",lty="solid",lwd=0.125)
dev.off()




# plot accuracies (narrow down and add texts, add error (range) bars)...
  # you can use "segments" to add bars in base graphics.
  # segments(x0, y0, x1 = x0, y1 = y0,vcol = par("fg"), lty = par("lty"), lwd = par("lwd"),...)

#e.g. Here "epsilon" controls the line across the top and the bottom of the line
#plot(x,y, ylim=(0,6))
#epsilon= 0.02
#for(i in 1:5){
#    up = y[i] + sd[i]
#    low = y[i] - sd[i]
#    segments(x[i],low,x[i],up)
#    segments(x[i]-epsilon, up, x[i]+epsilon, up)
#    segments(x[i]-epsilon, low, x[i]+epsilon, low)
#}


#  or simply use vectorized function calls
#segments(x,y-sd,x,y+sd)
#epsilon=0.02
#segments(x-epsilon,y-sd,x+epsilon,y-sd)
#segments(x-epsilon,y+sd,x+epsilon,y+sd)




png(filename="./figs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc4.png",
    height=1500,width=1500,res=350)
chartsToPlot <- c("fcbf.svm.BSResults",
                  #"cor.rpart.BSResults",
                  #"cor.svm.BSResults",
                  "su.rpart.BSResults",
                  #"ig.rpart.BSResults",
                  "ig.svm.BSResults"#,
                  #"gr.svm.BSResults"
                  )
ep = 1.25
plot(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     type="l",las=1,ylab="Accuracy",xlab="Features", col=1, ylim=c(0.76,0.81))
sd <- as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc[chartsToPlot[1],])
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) - sd,
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) + sd,
        col=1,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) - sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) - sd,
        col=1,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) + sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],]) + sd,
        col=1,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     gsub("0","",as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[1],])),
     col=1,cex=0.4,pos=1)
                                                                         
lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],], type="l", col=4)
sd <- as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc[chartsToPlot[2],])
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) - sd,
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) + sd,
        col=4,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) - sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) - sd,
        col=4,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) + sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],]) + sd,
        col=4,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[2],]),
     col=4,cex=0.4,pos=1)

lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],], type="l", col=6)
sd <- as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableSDAcc[chartsToPlot[3],])
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) - sd,
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) + sd,
        col=6,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) - sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) - sd,
        col=6,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) + sd,
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],]) + sd,
        col=6,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[3],]),
     col=6,cex=0.4,pos=1)


legend(x="topright",legend=chartsToPlot,col=c(1,4,6),cex=0.35, lty = "solid")
#grid(nx=10,lty="solid",lwd=0.25)
abline(h=c(seq(0.76,0.81,by=0.00125)),v=c(seq(20,180,2.5)),col="lightgray",lty="solid",lwd=0.125)
abline(h=c(seq(0.76,0.81,by=0.005)),v=c(seq(20,180,10)),col="gray",lty="solid",lwd=0.125)
dev.off()





png(filename="./figs/expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc5.png",
    height=1500,width=1500,res=350)
chartsToPlot <- c("fcbf.svm.BSResults",
                  #"cor.rpart.BSResults",
                  #"cor.svm.BSResults",
                  "su.rpart.BSResults",
                  #"ig.rpart.BSResults",
                  "ig.svm.BSResults"#,
                  #"gr.svm.BSResults"
                  )
ep = 1.25
plot(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     type="l",las=1,ylab="Accuracy",xlab="Features", col=1, ylim=c(0.65,0.90), xlim=c(0,250))
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[1],]),
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[1],]),
        col=1,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[1],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[1],]),
        col=1,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[1],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[1],]),
        col=1,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[1],],
     gsub("0","",as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[1],])),
     col=1,cex=0.4,pos=1)
                                                                         
lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],], type="l", col=4)
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[2],]),
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[2],]),
        col=4,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[2],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[2],]),
        col=4,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[2],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[2],]),
        col=4,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[2],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[2],]),
     col=4,cex=0.4,pos=1)

lines(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],], type="l", col=6)
segments(x0=seq(20,180,10),
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[3],]),
        x1=seq(20,180,10),
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[3],]),
        col=6,lty="solid",lwd=0.5) #add error bars...
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[3],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMinAcc[chartsToPlot[3],]),
        col=6,lty="solid",lwd=0.5) #add error bars upper bar caps
segments(x0=seq(20,180,10) - ep,
        y0=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[3],]),
        x1=seq(20,180,10) + ep,
        y1=as.numeric(expMatMRtWithBiClassInfo.topFeatsSpecResultsTableMaxAcc[chartsToPlot[3],]),
        col=6,lty="solid",lwd=0.5) #add error bars lower bar caps
text(seq(20,180,10),expMatMRtWithBiClassInfo.topFeatsSpecResultsTableAcc[chartsToPlot[3],],
     as.character(expMatMRtWithBiClassInfo.topFeatsSpecResultsTable[chartsToPlot[3],]),
     col=6,cex=0.4,pos=1)


legend(x="topright",legend=chartsToPlot,col=c(1,4,6),cex=0.35, lty = "solid")
#grid(nx=10,lty="solid",lwd=0.25)
abline(h=c(seq(0.65,0.90,by=0.00125)),v=c(seq(0,250,2.5)),col="lightgray",lty="solid",lwd=0.125)
abline(h=c(seq(0.65,0.90,by=0.005)),v=c(seq(0,250,10)),col="gray",lty="solid",lwd=0.125)
dev.off()

