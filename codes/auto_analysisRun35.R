# auto_analysisRun35.R
# Paul Aiyetan
# 2016/09/28



#  this analysis run attempts to run validations of other predictive models on
#    our independently curated dataset from the curatedBreastData package.
#    Thankfully these predicition models are implemneted in the "genefu" R package
#    
rm(list=ls())
library(genefu)
library(org.Hs.eg.db)
library(rmeta)
source("./codes/annotateExpressionMatrix.R")
#library(xtable)
#library(rmeta)
#library(Biobase)
#library(caret)

#source("http://www.bioconductor.org/biocLite.R")
#biocLite("breastCancerMAINZ")
#biocLite("breastCancerTRANSBIG")
#biocLite("breastCancerUPP")
#biocLite("breastCancerUNT")
#biocLite("breastCancerNKI")

#library(breastCancerMAINZ)
#library(breastCancerTRANSBIG)
#library(breastCancerUPP)
#library(breastCancerUNT)
#library(breastCancerNKI)



# load independent validataion dataset matrix.
load(file="./objs/inDataExpMatDFStWithBiClassInfo.rda")
# make annotation table object
inDataExpMatDFSt <- inDataExpMatDFStWithBiClassInfo[,-ncol(inDataExpMatDFStWithBiClassInfo)]
inDataExpMatENSEMBLIds <- dimnames(inDataExpMatDFSt)[[2]]
EntrezGene.ID <- convertIDs(inDataExpMatENSEMBLIds, "ENSEMBL", "ENTREZID", org.Hs.eg.db)
Gene.symbol <- convertIDs(inDataExpMatENSEMBLIds, "ENSEMBL", "SYMBOL", org.Hs.eg.db)
#Ensemble.ID <- convertIDs(inDataExpMatENSEMBLIds, "ENSEMBL", "ENSEMBL", org.Hs.eg.db)#to cross check mapping
Gene.Name <- convertIDs(inDataExpMatENSEMBLIds, "ENSEMBL", "GENENAME", org.Hs.eg.db)
Ensembl.ID <- inDataExpMatENSEMBLIds

inDataAnnot <- data.frame(Ensembl.ID,EntrezGene.ID,Gene.symbol,Gene.Name,stringsAsFactors=FALSE)
rownames(inDataAnnot) <- Ensembl.ID


# other prognostic indeces...
#1) OncotypeDx
oncotypeDX <- oncotypedx(data=inDataExpMatDFSt,annot=inDataAnnot,do.mapping=TRUE) #note incomplete mappings
#2) Mammaprint
GENE70 <- gene70(data=inDataExpMatDFSt,annot=inDataAnnot,do.mapping=TRUE,std="none") 
#3) GGI 
GGI <- ggi(data=inDataExpMatDFSt,annot=inDataAnnot,do.mapping=TRUE) #not incomplete mappings
#4) Gene76 (Veridex LLC)
#GENE76 <- gene76(data=inDataExpMatDFSt)# mainly for affymetrix platforms..requires ER status information


inValidationPerfsCompWithMappedEnsemblIds <-
   list('OncotypeDX'=oncotypeDX,
        'GENE70'=GENE70,
        'GGI'=GGI)

save(inValidationPerfsCompWithMappedEnsemblIds,
     file="./objs/inValidationPerfsCompWithMappedEnsemblIds.rda")


############################################################
####### Using Affy probe Ids...
# appears 'genefu' R package's prognostic maker works best on raw affymetrix probe id. There, reextract
#   these expression matrices without mapping/annotating to ensemble ids...
load(file="./objs/inDataExpressionSetsWithAffyProbeIds.rda")
source("./codes/combineExpressionSets.R")


# correct the time units in the dataset "GSE16446_GPL50_all" from days to months
pData(inDataExpressionSetsWithAffyProbeIds$GSE16446_GPL570_all)[,"DFS_months_or_MIN_months_of_DFS"] <-
    ((pData(inDataExpressionSetsWithAffyProbeIds$GSE16446_GPL570_all)[,
                         "DFS_months_or_MIN_months_of_DFS"]/365)*12)


inDataCombinedExpressionSetWithAffyProbeIds <-
    combineExpressionSets(esetsList=inDataExpressionSetsWithAffyProbeIds,
                          modifyColumnNames=FALSE)
save(inDataCombinedExpressionSetWithAffyProbeIds,
     file="./objs/inDataCombinedExpressionSetWithAffyProbeIds.rda")



# derive expMatDFStWithClassInfo
dfs.info <- pData(inDataCombinedExpressionSetWithAffyProbeIds)[,"DFS_months_or_MIN_months_of_DFS"]
inDataExpMatrixWithAffyIds <- exprs(inDataCombinedExpressionSetWithAffyProbeIds)
inDataExpMatWithAffyIdsDFS <- inDataExpMatrixWithAffyIds[,!is.na(dfs.info)]
inDataExpMatWithAffyIdsDFSt <- t(inDataExpMatWithAffyIdsDFS)
dfs.info <- dfs.info[!is.na(dfs.info)]

# with raw (months) class info
inDataExpMatWithAffyIdsDFStWithClassInfo <- cbind(inDataExpMatWithAffyIdsDFSt,dfs.info)
save(inDataExpMatWithAffyIdsDFStWithClassInfo,
     file = "./objs/inDataExpMatWithAffyIdsDFStWithClassInfo.rda")
load(file="./objs/inDataExpMatWithAffyIdsDFStWithClassInfo.rda")

# with BiClassInfo
dfs.info <- ifelse(dfs.info < 60, 1, 0)
inDataExpMatWithAffyIdsDFStWithBiClassInfo <- cbind(inDataExpMatWithAffyIdsDFSt,dfs.info)
save(inDataExpMatWithAffyIdsDFStWithBiClassInfo,
     file = "./objs/inDataExpMatWithAffyIdsDFStWithBiClassInfo.rda")
load(file="./objs/inDataExpMatWithAffyIdsDFStWithBiClassInfo.rda")


# inDataExpMatWithAffyIdsDFSt
source("./codes/runIndependentValidations.R")

#1) OncotypeDx
OncotypeDX <- oncotypedx(data=inDataExpMatWithAffyIdsDFSt,do.mapping=FALSE) #note
    # compute performance...
oncotypePred <- as.vector(OncotypeDX$risk)
oncotypePred2Compare <- oncotypePred[ifelse(oncotypePred==0.5,FALSE,TRUE)]
                                 # remove predictions classified as intermediate risk (0.5) from comparison
dfs.info.2Compare <- dfs.info[ifelse(oncotypePred==0.5,FALSE,TRUE)]
          # remove associated true values of samples classified as intermediate risk (0.5) from comparison
xt <- table(oncotypePred2Compare,dfs.info.2Compare) 
oncotypedx.perfs <- runPerfMeasures(xt)


oncotypedx.perfs
#$sens
#[1] 0.9252336
#
#$spec
#[1] 0.06451613
#
#$ppv
#[1] 0.8722467
#
#$npv
#[1] 0.1111111
#
#$acc
#[1] 0.8163265
#
#$err
#[1] 0.1836735


#2) Mammaprint
# derive annotations for affy probe ids..
require("hgu133plus2.db")
probeIds <- colnames(inDataExpMatWithAffyIdsDFSt)
annots <- getAnnotations(x=probeIds,metaData="hgu133plus2")
colnames(annots) <- gsub("EGID","EntrezGene.ID",colnames(annots))
GENE70 <- gene70(data=inDataExpMatWithAffyIdsDFSt,annot=annots,do.mapping=TRUE,std="none")
GENE70pred <- as.vector(GENE70$risk)
xt <- table(GENE70pred, dfs.info) 
GENE70pred.perfs <- runPerfMeasures(xt)


GENE70pred.perfs
#$sens
#[1] 0.9957627
#
#$spec
#[1] 0
#
#$ppv
#[1] 0.8671587
#
#$npv
#[1] 0
#
#$acc
#[1] 0.8639706
#
#$err
#[1] 0.1360294


#3) GGI
# requires informaton about histological grade of tumors
histG <- pData(inDataCombinedExpressionSetWithAffyProbeIds)[,"hist_grade"]
summary(as.factor(histG))
#   1    2    3 NA's 
#  31  164  237  365 
histG.dfs.info <- pData(inDataCombinedExpressionSetWithAffyProbeIds)[,"DFS_months_or_MIN_months_of_DFS"]
histG <- histG[!is.na(histG.dfs.info)]
summary(as.factor(histG))
#   1    2    3 NA's 
#  11   70  173   18 
GGI <- ggi(data=inDataExpMatWithAffyIdsDFSt,annot=annots,
           hg=histG, # vector of histological grade of the tumors...
           do.mapping=FALSE) #not incomplete mappings
GGIpred <- as.vector(GGI$risk)
xt <- table(GGIpred, dfs.info) 
GGIpred.perfs <- runPerfMeasures(xt)

GGIpred.perfs
#$sens
#[1] 0.5932203
#
#$spec
#[1] 0.5
#
#$ppv
#[1] 0.8860759
#
#$npv
#[1] 0.1578947
#
#$acc
#[1] 0.5808824
#
#$err
#[1] 0.4191176
#


#4) Gene76 (Veridex LLC)
# requires informaton about er status of tumors
eR <- pData(inDataCombinedExpressionSetWithAffyProbeIds)[,"ER_preTrt"]#"ER_preTrt","ER_expr_preTrt"
summary(as.factor(eR))
#   0    1 NA's 
# 365  378   54 
eR.dfs.info <- pData(inDataCombinedExpressionSetWithAffyProbeIds)[,"DFS_months_or_MIN_months_of_DFS"]
eR <- eR[!is.na(eR.dfs.info)]
summary(as.factor(eR))
#   0    1 NA's 
# 168  103    1 
GENE76 <- gene76(data=inDataExpMatWithAffyIdsDFSt, er = eR)
                                        # mainly for affymetrix platforms..requires ER status information
GENE76pred <- as.vector(GENE76$risk)
xt <- table(GENE76pred, dfs.info) 
GENE76pred.perfs <- runPerfMeasures(xt)

GENE76pred.perfs
#$sens
#[1] 1
#
#$spec
#[1] 0.02777778
#
#$ppv
#[1] 0.8703704
#
#$npv
#[1] 1
#
#$acc
#[1] 0.8708487
#
#$err
#[1] 0.1291513



inValidationPerfsCompWithAffyIds <-
   list('OncotypeDX'=OncotypeDX,
        'GENE70'=GENE70,
        'GGI'=GGI,
        'GENE76'=GENE76)

save(inValidationPerfsCompWithAffyIds,file="./objs/inValidationPerfsCompWithAffyIds.rda")
load(file="./objs/inValidationPerfsCompWithAffyIds.rda")

inValidationPredPerfsComparisonTable <- rbind(oncotypedx.perfs,GENE70pred.perfs,
                                              GGIpred.perfs,GENE76pred.perfs)

save(inValidationPredPerfsComparisonTable,file="./objs/inValidationPredPerfsComparisonTable.rda")
#inValidationPredPerfsComparisonTable <- as.data.frame(model=c("OncotypeDX","GENE70","GGI","GENE76"),
#                       inValidationPredPerfsComparisonTable, stringsAsFactors=FALSE)
write.table(inValidationPredPerfsComparisonTable,file="./texts/inValidationPredPerfsComparisonTable.tsv",
            quote=FALSE,sep="\t",row.names=TRUE)






###### compute C-Index(ces) ################
# concordance.index(...)
load(file="./objs/inValidationPerfsCompWithAffyIds.rda")
source("./codes/computeConcordanceIndeces.R")
phenoTable <- pData(inDataCombinedExpressionSetWithAffyProbeIds)
pheno.attr <- "DFS_months_or_MIN_months_of_DFS"

inValidationCIndeces.others <-
     lapply(inValidationPerfsCompWithAffyIds, function(x,y=phenoTable,z=pheno.attr){
         sampleIds <- names(x$score)
         rscores <- as.numeric(x$score)
         cI <- computeConcordanceIndex(sampleIds=sampleIds,
                                       rscores=rscores,phenoTable=y,pheno.attr=z)
         return(cI)
         })

# save
save(inValidationCIndeces.others, file="./objs/inValidationCIndeces.others.rda")
load(file="./objs/inValidationCIndeces.others.rda")

# write
inValidationCIndecesTable.others <-
    as.data.frame(do.call("rbind",inValidationCIndeces.others),stringsAsFactors=FALSE)
write.table(inValidationCIndecesTable.others, file="./texts/inValidationCIndecesTable.others.tsv",
            sep="\t",quote=FALSE)

# plot
cI <- inValidationCIndecesTable.others
png(filename="./figs/inValidationCIndecesTable.others.png",
        height=3000, width=3000, res=350)
metaplot.surv(mn=cI$cindex,lower=cI$lower,upper=cI$upper,labels=rownames(cI),xlim=c(0.3,0.9),
             boxsize=0.5, zero=0.5,col=meta.colors(box="royalblue",line="darkblue",zero="firebrick"),
             main=paste("Concordance indeces"),cex.main=0.95)
dev.off()






# Visualize a comparison of the concordance index obtained from other measures against
#  our in-house proposed method...

load(file="./objs/inValidationConcordanceIndeces.rda")
MDL <- inValidationConcordanceIndeces$lr.coxph.w_su.rpart.BSResults.90
inValidationCIndecesTable <- rbind(inValidationCIndecesTable.others, MDL)
rownames(inValidationCIndecesTable) <- c("OncotypeDX","Mammaprint","GGI (MapQuant)", "Veridex LLC","MDL")
# plot
cI <- inValidationCIndecesTable
png(filename="./figs/inValidationCIndecesTable.compare.png",
        height=2500, width=2500, res=350)
metaplot.surv(mn=cI$cindex,lower=cI$lower,upper=cI$upper,labels=rownames(cI),xlim=c(0.3,0.9),
             boxsize=0.5, zero=0.5,col=meta.colors(box="royalblue",line="darkblue",zero="firebrick"),
             main=paste("Concordance indeces"),cex.main=0.95)
grid(lwd=0.5,lty="solid",col="lightgrey")
#text(as.character(cI$p.value)
dev.off()



##### Aggregate/combine C-Indeces ##################
  # combine.est(...)
  # cindex.comp.meta(...)
