# auto_analysisRun14.R
# Paul Aiyetan
# 08/30/2016


# Here we look for informative features within a 2-class (low-risk vs high-risk) space most
#      predictive of metastasis.

rm(list=ls())
require(Biobase)
require("FSelector")
require("Biocomb") # contains the 'FCBF' feature selection algorithm
require("discretization")

load("./objs/allMergedNoDupsExpressionSetEBayesAdjusted.rda", verbose=TRUE)
load("./objs/allMergedNoDupsDatasetsPDataTable.rda", verbose=TRUE)

# from expression matrix, subset exprs of samples with
#     non-missing information in the 'lr.time' attribute...
expMat <- exprs(allMergedNoDupsExpressionSetEBayesAdjusted)
mr.time.vec <- as.numeric(allMergedNoDupsDatasetsPDataTable$mr.time)
samples.without.nas <- colnames(expMat)[!is.na(mr.time.vec)]
expMatMR <- expMat[,samples.without.nas]
mr.time.vec <- as.numeric(allMergedNoDupsDatasetsPDataTable[samples.without.nas,"mr.time"])
# discretize lr.time.vec and make as.factor object
mr.time.vec <-  unlist(lapply(mr.time.vec,function(x){
                              if(x < 60){
                                  return(1)
                              }else{
                                  return(2)                             
                              }}))
mr.time.class <- as.factor(mr.time.vec)
expMatMRtransposed <- t(expMatMR)
expMatMRtWithBiClassInfo <- cbind(expMatMRtransposed,mr.time.class)

# save class-info-contatining table object
save(expMatMRtWithBiClassInfo, file="./objs/expMatMRtWithBiClassInfo.rda")
load(file="./objs/expMatMRtWithBiClassInfo.rda")



#########################
# Discretize the data.
#########################
  # having the liberty to se either the biocomb implementation or the discretization packages implementation,
     # it was observed that the discretization packages's implementation is much more intuittive, and
     # closer to the original description than the biocomb's package implementation of the
     # Chi2 algorithm.
# expMatLRtWithClassInfo.disc.chi2 <- chi2(expMatLRtWithClassInfo,0.5,0.05)
     # the R implementation of the chi-squared algorithm from both R packages 'discretization' (chi2) and
        # 'FSelector' (chi-squared) run extremely slow and with indeterminate efficiency when ran against
        #  very large dataset...

        # To address this, the weka Java platform is employed here. However, the weKa platform requires
        #  the .ARFF file input format.
source("./codes/createWekaARFFInputFile.R",verbose=TRUE)
createWekaARFFInputFile(expMatMRtWithBiClassInfo,outputFilePath="./texts/expMatMRtWithBiClassInfo.arff")



       ##########################################
       #   i. run Weka CAIM discretization on expMatMRtWithBiClassInfo.arff
       #  ii. save derived discretized file in "./texts/expMatMRtWithBiClassInfo.disc-CAIM.tsv"
       #      OR save derived discretized data file as CSV in "./text/expMatMRtWithBiClassInfo.disc-CAIM.csv"


# --- reading saved .csv file --- #
# NOTE: Weka provides an optional capability to save discretized data as ".csv". Since in the case of the
# .csv file, there are no added notations other than discretized data, we can simply read-in the data
# back into R using the "read.csv" function.
expMatMRtWithBiClassInfo.disc.CAIM <-
   read.csv("./texts/expMatMRtWithBiClassInfo.disc-CAIM.csv", sep=",", header=TRUE, stringsAsFactors=FALSE)
rownames(expMatMRtWithBiClassInfo.disc.CAIM) <- rownames(expMatMRtWithBiClassInfo)
head(expMatMRtWithBiClassInfo.disc.CAIM)[,c(1,2,ncol(expMatMRtWithBiClassInfo.disc.CAIM))]# view

save(expMatMRtWithBiClassInfo.disc.CAIM,file="./objs/expMatMRtWithBiClassInfo.disc.CAIM.rda")
load(file="./objs/expMatMRtWithBiClassInfo.disc.CAIM.rda")



########################
# Compute filter measures for discretized data...
#   and save in file(s)
#      i) "./texts/expMatMRtWithBiClassInfo.RankedFeatures.Chi2"
#     ii) "./texts/expMatMRtWithBiClassInfo.RankedFeatures.IG"
#    iii) "./texts/expMatMRtWithBiClassInfo.RankedFeatures.GR"
#     iv) "./texts/expMatMRtWithBiClassInfo.RankedFeatures.SU"
#    
#########################
    # including (computed in Weka):
    # 1) Chi-squared
    # 2) Information Gain
    # 3) Gain-Ratio
    # 4) Symmetrical Uncertainty
# read-in derived values...

   # define a helper function to extract values from 'string' lines with admixture of spaces and values
   getLineValues <-
       function(x){
         X <- unlist(strsplit(x," "))
         X.logical <- X != ""
         X <- X[X.logical]
         return(X)}


# Chi-squared...
expMatMRtWithBiClassInfo.RankedFeatures.chi2 <-
    readLines("./texts/expMatMRtWithBiClassInfo.RankedFeatures.Chi2")
expMatMRtWithBiClassInfo.RankedFeatures.chi2 <-
    lapply(expMatMRtWithBiClassInfo.RankedFeatures.chi2, getLineValues)
expMatMRtWithBiClassInfo.RankedFeatures.chi2 <-
    as.data.frame(do.call("rbind",expMatMRtWithBiClassInfo.RankedFeatures.chi2),stringsAsFactors=FALSE)
colnames(expMatMRtWithBiClassInfo.RankedFeatures.chi2) <- c("Value","FeatureIndex","Transcript")
expMatMRtWithBiClassInfo.RankedFeatures.chi2$Value <-
    as.numeric(expMatMRtWithBiClassInfo.RankedFeatures.chi2$Value)
save(expMatMRtWithBiClassInfo.RankedFeatures.chi2,
     file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.chi2.rda")# save
load(file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.chi2.rda")


# Information Gain...
expMatMRtWithBiClassInfo.RankedFeatures.ig <-
    readLines("./texts/expMatMRtWithBiClassInfo.RankedFeatures.IG")
expMatMRtWithBiClassInfo.RankedFeatures.ig <-
    lapply(expMatMRtWithBiClassInfo.RankedFeatures.ig, getLineValues)
expMatMRtWithBiClassInfo.RankedFeatures.ig <-
    as.data.frame(do.call("rbind",expMatMRtWithBiClassInfo.RankedFeatures.ig),stringsAsFactors=FALSE)
colnames(expMatMRtWithBiClassInfo.RankedFeatures.ig) <- c("Value","FeatureIndex","Transcript")
expMatMRtWithBiClassInfo.RankedFeatures.ig$Value <-
    as.numeric(expMatMRtWithBiClassInfo.RankedFeatures.ig$Value)
save(expMatMRtWithBiClassInfo.RankedFeatures.ig,
     file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.ig.rda")# save
load(file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.ig.rda")


# Gain Ratio...
expMatMRtWithBiClassInfo.RankedFeatures.gr <-
    readLines("./texts/expMatMRtWithBiClassInfo.RankedFeatures.GR")
expMatMRtWithBiClassInfo.RankedFeatures.gr <-
    lapply(expMatMRtWithBiClassInfo.RankedFeatures.gr, getLineValues)
expMatMRtWithBiClassInfo.RankedFeatures.gr <-
    as.data.frame(do.call("rbind",expMatMRtWithBiClassInfo.RankedFeatures.gr),stringsAsFactors=FALSE)
colnames(expMatMRtWithBiClassInfo.RankedFeatures.gr) <- c("Value","FeatureIndex","Transcript")
expMatMRtWithBiClassInfo.RankedFeatures.gr$Value <-
    as.numeric(expMatMRtWithBiClassInfo.RankedFeatures.gr$Value)
save(expMatMRtWithBiClassInfo.RankedFeatures.gr,
     file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.gr.rda")# save
load(file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.gr.rda")


# Symmetrical Uncertainty...
expMatMRtWithBiClassInfo.RankedFeatures.su <-
    readLines("./texts/expMatMRtWithBiClassInfo.RankedFeatures.SU")
expMatMRtWithBiClassInfo.RankedFeatures.su <-
    lapply(expMatMRtWithBiClassInfo.RankedFeatures.su, getLineValues)
expMatMRtWithBiClassInfo.RankedFeatures.su <-
    as.data.frame(do.call("rbind",expMatMRtWithBiClassInfo.RankedFeatures.su),stringsAsFactors=FALSE)
colnames(expMatMRtWithBiClassInfo.RankedFeatures.su) <- c("Value","FeatureIndex","Transcript")
expMatMRtWithBiClassInfo.RankedFeatures.su$Value <-
    as.numeric(expMatMRtWithBiClassInfo.RankedFeatures.su$Value)
save(expMatMRtWithBiClassInfo.RankedFeatures.su,
     file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.su.rda")# save
load(file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.su.rda")


      ########################
      # As an alternate to filter measures, simply directly 
      #    Employ the FCBF algorithm on subset of data....(to minimize redundancy - a step which otherwise
      #         would have been employed in the learning algorithm step within the wrapper method)
      ########################

      source("./codes/createFCBFJavaInputFiles.R", verbose=TRUE)
      createFCBFJavaInputFiles(expMatMRtWithBiClassInfo.disc.CAIM,
                         outputFilePath="./texts/expMatMRtWithBiClassInfo.disc.CAIM")

        ###########################################
        #   i. run the FCBF selection algorithm on dataset
        #  ii. save features that pass <delta> (symmetrical uncertainty above a threshold) in
        #        "./texts/expMatMRtWithBiClassInfo.disc.CAIM.fcbf"
     
        #   ...
        #  save selected Features in the file "./texts/expMatMRtWithBiClassInfo.SelectedFeatures.fcbf"

       # read-in derived values/seected features...
       expMatMRtWithBiClassInfo.SelectedFeatures.fcbf <-
            read.table("./texts/expMatMRtWithBiClassInfo.SelectedFeatures.fcbf",
                       header=T,sep=" ",stringsAsFactors=FALSE)
       head(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf) # view
       save(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf,
            file="./objs/expMatMRtWithBiClassInfo.SelectedFeatures.fcbf.rda")# save
       load(file="./objs/expMatMRtWithBiClassInfo.SelectedFeatures.fcbf.rda")




######################
# Employ wrappers to select optimal features...
######################
  # the wrapper approaches described in the FSelector package...
  # 1) exhaustive.search
  # 2) hill.climbing.search
  # 3) best.first.search, and the
  # 4) greedy.search approaches
  #    a) forward.search
  #    b) backward.search

require(rpart)
require(FSelector)
source("./codes/runFeatureSelection.R", verbose=TRUE)

################################################
# Initial features selected and ranked by FCBF
rankedFeaturesTableList <- list(fcbf=expMatMRtWithBiClassInfo.SelectedFeatures.fcbf)
# undiscretized expression data....
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTableList,
                 expMatWithClassInfo=expMatMRtWithBiClassInfo)
rankedFeatsCrossValidResults.MRBiClass.rpart.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
rankedFeatsCrossValidResults.MRBiClass.nb.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
rankedFeatsCrossValidResults.MRBiClass.svm.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
# save (undiscretized)
save(rankedFeatsCrossValidResults.MRBiClass.rpart.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.rpart.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.MRBiClass.nb.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.nb.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.MRBiClass.svm.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.svm.k10.FCBF.rda")

load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.rpart.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.nb.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.svm.k10.FCBF.rda")


# discretized expression data..
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTableList,
                 expMatWithClassInfo=expMatMRtWithBiClassInfo.disc.CAIM)
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=nrow(expMatMRtWithBiClassInfo.SelectedFeatures.fcbf))# all 30 features..
# save (discretized)
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.FCBF.rda")

load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.FCBF.rda")


#################################################
# Feature selected by other ranking algorithms
#   ....include the correlation based method.


# load the ranking derived by correlation...
load("./objs/features.mr.corr.table.ordered.rda", verbose=TRUE)
head(features.mr.corr.table.ordered)
expMatMRtWithBiClassInfo.RankedFeatures.cor <- features.mr.corr.table.ordered[,c("estimate.abs",
                                                                            "estimate",
                                                                            "features")]
colnames(expMatMRtWithBiClassInfo.RankedFeatures.cor) <- c("Value","Estimate","Transcript")
save(expMatMRtWithBiClassInfo.RankedFeatures.cor,file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.cor.rda")
load(file="./objs/expMatMRtWithBiClassInfo.RankedFeatures.cor.rda")

rankedFeaturesTablesList <-
    list(
         cor=expMatMRtWithBiClassInfo.RankedFeatures.cor,
         chi2=expMatMRtWithBiClassInfo.RankedFeatures.chi2,
         su=expMatMRtWithBiClassInfo.RankedFeatures.su,
         ig=expMatMRtWithBiClassInfo.RankedFeatures.ig,
         gr=expMatMRtWithBiClassInfo.RankedFeatures.gr
        ) # reconstitute rankedFeaturesTablesList...

# undiscretized expression data...
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTablesList,
                 expMatWithClassInfo=expMatMRtWithBiClassInfo)                

rankedFeatsCrossValidResults.MRBiClass.rpart.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=100)

rankedFeatsCrossValidResults.MRBiClass.nb.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=100)

rankedFeatsCrossValidResults.MRBiClass.svm.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=100)
# save (undiscretized)
save(rankedFeatsCrossValidResults.MRBiClass.rpart.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.rpart.k10.rda")
save(rankedFeatsCrossValidResults.MRBiClass.nb.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.nb.k10.rda")
save(rankedFeatsCrossValidResults.MRBiClass.svm.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.svm.k10.rda")

load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.rpart.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.nb.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.svm.k10.rda")


# discretized expression data...
rankedFeaturesTablesList <-
    list(
         cor=expMatMRtWithBiClassInfo.RankedFeatures.cor,
         chi2=expMatMRtWithBiClassInfo.RankedFeatures.chi2,
         su=expMatMRtWithBiClassInfo.RankedFeatures.su,
         ig=expMatMRtWithBiClassInfo.RankedFeatures.ig,
         gr=expMatMRtWithBiClassInfo.RankedFeatures.gr
        ) # reconstitute rankedFeaturesTablesList...

dataList <- list(rankedFeaturesTablesList=rankedFeaturesTablesList,
                 expMatWithClassInfo=expMatMRtWithBiClassInfo.disc.CAIM)
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=100)
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=100)
rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=100)
# save (discretized)
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.rda")
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.rda")
save(rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10,
     file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.rda")

load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.rpart.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.nb.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.MRBiClass.CAIMDisc.svm.k10.rda")


