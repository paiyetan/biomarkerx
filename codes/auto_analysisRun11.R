# auto_analysisRun11.R
# Paul Aiyetan
# 08/12/2016




# Here we begin the real supervised prediction...First, we'll like to most informative
#   features for predicting recurrence.
rm(list=ls())
require(Biobase)
require("FSelector")
require("Biocomb") # contains the 'FCBF' feature selection algorithm
require("discretization")

load("./objs/allMergedNoDupsExpressionSetEBayesAdjusted.rda", verbose=TRUE)
load("./objs/allMergedNoDupsDatasetsPDataTable.rda", verbose=TRUE)

# from expression matrix, subset exprs of samples with
#     non-missing information in the 'lr.time' attribute...
expMat <- exprs(allMergedNoDupsExpressionSetEBayesAdjusted)
lr.time.vec <- as.numeric(allMergedNoDupsDatasetsPDataTable$lr.time)
samples.without.nas <- colnames(expMat)[!is.na(lr.time.vec)]
expMatLR <- expMat[,samples.without.nas]
lr.time.vec <- as.numeric(allMergedNoDupsDatasetsPDataTable[samples.without.nas,"lr.time"])
# discretize lr.time.vec and make as.factor object
lr.time.vec <-  unlist(lapply(lr.time.vec,function(x){
                              if(x <= 36){
                                  return(1)
                              }else if((x > 36)&(x <= 60)){
                                  return(2)
                              }else{
                                  return(3)                             
                              }}))
lr.time.class <- as.factor(lr.time.vec)
expMatLRtransposed <- t(expMatLR)
expMatLRtWithClassInfo <- cbind(expMatLRtransposed,lr.time.class)

# save class-info-contatining table object
save(expMatLRtWithClassInfo, file="./objs/expMatLRtWithClassInfo.rda")
load(file="./objs/expMatLRtWithClassInfo.rda")


# select features using the FCBF algorithm and the "MDL" discretization method...
# expMatLR.FCBF.MDL.feats1 <- select.fast.filter(expMatLRtWithClassInfo,disc.method="MDL",threshold=0.2,
#                                     attrs.nominal=numeric())
# ERROR in processing:   Process R segmentation fault (core dumped) at Thu Jul  7 16:36:16 2016
#     probably due to exceeding the amount of memory space available..Therfore, it is thought to
#     execute the intermediate steps individually filtering along the way to  smaller subset...





#########################
# Discretize the data.
#########################
  # having the liberty to se either the biocomb implementation or the discretization packages implementation,
     # it was observed that the discretization packages's implementation is much more intuittive, and
     # closer to the original description than the biocomb's package implementation of the
     # Chi2 algorithm.
# expMatLRtWithClassInfo.disc.chi2 <- chi2(expMatLRtWithClassInfo,0.5,0.05)
     # the R implementation of the chi-squared algorithm from both R packages 'discretization' (chi2) and
        # 'FSelector' (chi-squared) run extremely slow and with indeterminate efficiency when ran against
        #  very large dataset...

        # To address this, the weka Java platform is employed here. However, the weKa platform requires
        #  the .ARFF file input format.
source("./codes/createWekaARFFInputFile.R",verbose=TRUE)
createWekaARFFInputFile(expMatLRtWithClassInfo,outputFilePath="./texts/expMatLRtWithClassInfo.arff")





       ##########################################
       #   i. run Weka CAIM discretization on expMatLRtWithClassInfo.arff
       #  ii. save derived discretized file in "./texts/expMatLRtWithClassInfo.disc-CAIM.tsv"
       #      OR save derived discretized data file as CSV in "./text/expMatLRtWithClassInfo.disc-CAIM.csv"

# --- if reading saved .tsv file --- #
# expMatLRtWithClassInfo.disc.CAIM.tsv <-
#    read.table("./texts/expMatLRtWithClassInfo.disc-CAIM.tsv",sep="\t",header=TRUE,stringsAsFactors=FALSE)
# head(expMatLRtWithClassInfo.disc.CAIM.tsv)[,c(1:3,ncol(expMatLRtWithClassInfo.disc.CAIM.tsv))]# view
#     # modify data table as appropriate...
# rownames(expMatLRtWithClassInfo.disc.CAIM.tsv) <- rownames(expMatLRtWithClassInfo)
# expMatLRtWithClassInfo.disc.CAIM.tsv <- expMatLRtWithClassInfo.disc.CAIM.tsv[,-1]
# head(expMatLRtWithClassInfo.disc.CAIM.tsv)[,c(1:3,ncol(expMatLRtWithClassInfo.disc.CAIM.tsv))]# view

# --- if reading saved .csv file --- #
# NOTE: Weka provides an optional capability to save discretized data as ".csv" whereas the above
#   .tsv file was derived from copying a 'view' of the discretized data... Since in the case of the
# .csv file, there are added notations other than discretized data, we can simply read-in the data
# back into R using the "read.csv" function.
expMatLRtWithClassInfo.disc.CAIM <-
   read.csv("./texts/expMatLRtWithClassInfo.disc-CAIM.csv", sep=",", header=TRUE, stringsAsFactors=FALSE)
rownames(expMatLRtWithClassInfo.disc.CAIM) <- rownames(expMatLRtWithClassInfo)
head(expMatLRtWithClassInfo.disc.CAIM)[,c(1,2,ncol(expMatLRtWithClassInfo.disc.CAIM))]# view

save(expMatLRtWithClassInfo.disc.CAIM,file="./objs/expMatLRtWithClassInfo.disc.CAIM.rda")
load(file="./objs/expMatLRtWithClassInfo.disc.CAIM.rda")



########################
# Compute filter measures for discretized data...
#    
#########################
    # including (computed in Weka):
    # 1) Chi-squared
    # 2) Information Gain
    # 3) Gain-Ratio
    # 4) Symmetrical Uncertainty
# read-in derived values...

   # define a helper function to extract values from 'string' lines with admixture of spaces and values
   getLineValues <-
       function(x){
         X <- unlist(strsplit(x," "))
         X.logical <- X != ""
         X <- X[X.logical]
         return(X)}


# Chi-squared...
expMatLRtWithClassInfo.RankedFeatures.chi2 <-
    readLines("./texts/expMatLRtWithClassInfo.RankedFeatures.Chi2")
expMatLRtWithClassInfo.RankedFeatures.chi2 <-
    lapply(expMatLRtWithClassInfo.RankedFeatures.chi2, getLineValues)
expMatLRtWithClassInfo.RankedFeatures.chi2 <-
    as.data.frame(do.call("rbind",expMatLRtWithClassInfo.RankedFeatures.chi2),stringsAsFactors=FALSE)
colnames(expMatLRtWithClassInfo.RankedFeatures.chi2) <- c("Value","FeatureIndex","Transcript")
expMatLRtWithClassInfo.RankedFeatures.chi2$Value <-
    as.numeric(expMatLRtWithClassInfo.RankedFeatures.chi2$Value)
save(expMatLRtWithClassInfo.RankedFeatures.chi2,
     file="./objs/expMatLRtWithClassInfo.RankedFeatures.chi2.rda")# save
load(file="./objs/expMatLRtWithClassInfo.RankedFeatures.chi2.rda")


# Information Gain...
expMatLRtWithClassInfo.RankedFeatures.ig <-
    readLines("./texts/expMatLRtWithClassInfo.RankedFeatures.IG")
expMatLRtWithClassInfo.RankedFeatures.ig <-
    lapply(expMatLRtWithClassInfo.RankedFeatures.ig, getLineValues)
expMatLRtWithClassInfo.RankedFeatures.ig <-
    as.data.frame(do.call("rbind",expMatLRtWithClassInfo.RankedFeatures.ig),stringsAsFactors=FALSE)
colnames(expMatLRtWithClassInfo.RankedFeatures.ig) <- c("Value","FeatureIndex","Transcript")
expMatLRtWithClassInfo.RankedFeatures.ig$Value <-
    as.numeric(expMatLRtWithClassInfo.RankedFeatures.ig$Value)
save(expMatLRtWithClassInfo.RankedFeatures.ig,
     file="./objs/expMatLRtWithClassInfo.RankedFeatures.ig.rda")# save
load(file="./objs/expMatLRtWithClassInfo.RankedFeatures.ig.rda")


# Gain Ratio...
expMatLRtWithClassInfo.RankedFeatures.gr <-
    readLines("./texts/expMatLRtWithClassInfo.RankedFeatures.GR")
expMatLRtWithClassInfo.RankedFeatures.gr <-
    lapply(expMatLRtWithClassInfo.RankedFeatures.gr, getLineValues)
expMatLRtWithClassInfo.RankedFeatures.gr <-
    as.data.frame(do.call("rbind",expMatLRtWithClassInfo.RankedFeatures.gr),stringsAsFactors=FALSE)
colnames(expMatLRtWithClassInfo.RankedFeatures.gr) <- c("Value","FeatureIndex","Transcript")
expMatLRtWithClassInfo.RankedFeatures.gr$Value <-
    as.numeric(expMatLRtWithClassInfo.RankedFeatures.gr$Value)
save(expMatLRtWithClassInfo.RankedFeatures.gr,
     file="./objs/expMatLRtWithClassInfo.RankedFeatures.gr.rda")# save
load(file="./objs/expMatLRtWithClassInfo.RankedFeatures.gr.rda")


# Symmetrical Uncertainty...
expMatLRtWithClassInfo.RankedFeatures.su <-
    readLines("./texts/expMatLRtWithClassInfo.RankedFeatures.SU")
expMatLRtWithClassInfo.RankedFeatures.su <-
    lapply(expMatLRtWithClassInfo.RankedFeatures.su, getLineValues)
expMatLRtWithClassInfo.RankedFeatures.su <-
    as.data.frame(do.call("rbind",expMatLRtWithClassInfo.RankedFeatures.su),stringsAsFactors=FALSE)
colnames(expMatLRtWithClassInfo.RankedFeatures.su) <- c("Value","FeatureIndex","Transcript")
expMatLRtWithClassInfo.RankedFeatures.su$Value <-
    as.numeric(expMatLRtWithClassInfo.RankedFeatures.su$Value)
save(expMatLRtWithClassInfo.RankedFeatures.su,
     file="./objs/expMatLRtWithClassInfo.RankedFeatures.su.rda")# save
load(file="./objs/expMatLRtWithClassInfo.RankedFeatures.su.rda")


      ########################
      # As an alternate to filter measures, simply directly 
      #    Employ the FCBF algorithm on subset of data....(to minimize redundancy - a step which otherwise
      #         would have been employed in the learning algorithm step within the wrapper method)
      ########################

      source("./codes/createFCBFJavaInputFiles.R", verbose=TRUE)
      createFCBFJavaInputFiles(expMatLRtWithClassInfo.disc.CAIM,
                         outputFilePath="./texts/expMatLRtWithClassInfo.disc.CAIM")

        ###########################################
        #   i. run the FCBF selection algorithm on dataset
        #  ii. save features that pass <delta> (symmetrical uncertainty above a threshold) in
        #        "./texts/expMatLRtWithClassInfo.disc.CAIM.fcbf"
     
        #   ...
        #  save selected Features in the file "./texts/expMatLRtWithClassInfo.SelectedFeatures.fcbf"

       # read-in derived values/seected features...
       expMatLRtWithClassInfo.SelectedFeatures.fcbf <-
            read.table("./texts/expMatLRtWithClassInfo.SelectedFeatures.fcbf",
                       header=T,sep=" ",stringsAsFactors=FALSE)
       head(expMatLRtWithClassInfo.SelectedFeatures.fcbf) # view
       save(expMatLRtWithClassInfo.SelectedFeatures.fcbf,
            file="./objs/expMatLRtWithClassInfo.SelectedFeatures.fcbf.rda")# save
       load(file="./objs/expMatLRtWithClassInfo.SelectedFeatures.fcbf.rda")






######################
# Employ wrappers to select optimal features...
######################
  # the wrapper approaches described in the FSelector package...
  # 1) exhaustive.search
  # 2) hill.climbing.search
  # 3) best.first.search, and the
  # 4) greedy.search approaches
  #    a) forward.search
  #    b) backward.search

require(rpart)
require(FSelector)
source("./codes/runFeatureSelection.R", verbose=TRUE)

################################################
# Initial features selected and ranked by FCBF
rankedFeaturesTableList <- list(fcbf=expMatLRtWithClassInfo.SelectedFeatures.fcbf)
# undiscretized expression data....
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTableList,
                 expMatWithClassInfo=expMatLRtWithClassInfo)
rankedFeatsCrossValidResults.rpart.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
rankedFeatsCrossValidResults.nb.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
rankedFeatsCrossValidResults.svm.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
# save (undiscretized)
save(rankedFeatsCrossValidResults.rpart.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.rpart.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.nb.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.nb.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.svm.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.svm.k10.FCBF.rda")

load(file="./objs/rankedFeatsCrossValidResults.rpart.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.nb.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.svm.k10.FCBF.rda")


# discretized expression data..
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTableList,
                 expMatWithClassInfo=expMatLRtWithClassInfo.disc.CAIM)
rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
rankedFeatsCrossValidResults.CAIMDisc.nb.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
rankedFeatsCrossValidResults.CAIMDisc.svm.k10.FCBF <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=nrow(expMatLRtWithClassInfo.SelectedFeatures.fcbf))# all 34 features..
# save (discretized)
save(rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.CAIMDisc.nb.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.nb.k10.FCBF.rda")
save(rankedFeatsCrossValidResults.CAIMDisc.svm.k10.FCBF,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.svm.k10.FCBF.rda")

load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.nb.k10.FCBF.rda")
load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.svm.k10.FCBF.rda")



#################################################
# Feature selected by other ranking algorithms
#   ....include the correlation based method.


# load the ranking derived by correlation...
load("./objs/features.corr.table.ordered.rda", verbose=TRUE)
head(features.corr.table.ordered)
expMatLRtWithClassInfo.RankedFeatures.cor <- features.corr.table.ordered[,c("estimate.abs",
                                                                            "estimate",
                                                                            "features")]
colnames(expMatLRtWithClassInfo.RankedFeatures.cor) <- c("Value","Estimate","Transcript")
save(expMatLRtWithClassInfo.RankedFeatures.cor,file="./objs/expMatLRtWithClassInfo.RankedFeatures.cor.rda")
load(file="./objs/expMatLRtWithClassInfo.RankedFeatures.cor.rda")

rankedFeaturesTablesList <-
    list(
         cor=expMatLRtWithClassInfo.RankedFeatures.cor,
         chi2=expMatLRtWithClassInfo.RankedFeatures.chi2,
         su=expMatLRtWithClassInfo.RankedFeatures.su,
         ig=expMatLRtWithClassInfo.RankedFeatures.ig,
         gr=expMatLRtWithClassInfo.RankedFeatures.gr
        ) # reconstitute rankedFeaturesTablesList...

# undiscretized expression data...
dataList <- list(rankedFeaturesTablesList=rankedFeaturesTablesList,
                 expMatWithClassInfo=expMatLRtWithClassInfo)                

rankedFeatsCrossValidResults.rpart.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=100)

rankedFeatsCrossValidResults.nb.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=100)

rankedFeatsCrossValidResults.svm.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=100)
# save (undiscretized)
save(rankedFeatsCrossValidResults.rpart.k10,
     file="./objs/rankedFeatsCrossValidResults.rpart.k10.rda")
save(rankedFeatsCrossValidResults.nb.k10,
     file="./objs/rankedFeatsCrossValidResults.nb.k10.rda")
save(rankedFeatsCrossValidResults.svm.k10,
     file="./objs/rankedFeatsCrossValidResults.svm.k10.rda")

load(file="./objs/rankedFeatsCrossValidResults.rpart.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.nb.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.svm.k10.rda")


# discretized expression data...
rankedFeaturesTablesList <-
    list(
         cor=expMatLRtWithClassInfo.RankedFeatures.cor,
         chi2=expMatLRtWithClassInfo.RankedFeatures.chi2,
         su=expMatLRtWithClassInfo.RankedFeatures.su,
         ig=expMatLRtWithClassInfo.RankedFeatures.ig,
         gr=expMatLRtWithClassInfo.RankedFeatures.gr
        ) # reconstitute rankedFeaturesTablesList...

dataList <- list(rankedFeaturesTablesList=rankedFeaturesTablesList,
                 expMatWithClassInfo=expMatLRtWithClassInfo.disc.CAIM)
rankedFeatsCrossValidResults.CAIMDisc.rpart.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="rpart",
                        topFeatures=100)
rankedFeatsCrossValidResults.CAIMDisc.nb.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="nb",
                        topFeatures=100)
rankedFeatsCrossValidResults.CAIMDisc.svm.k10 <-
    runFeatureSelection(dataList,type="supervised",kFold=10,
                        seed=1,
                        eval.algorithm="svm",
                        topFeatures=100)
# save (discretized)
save(rankedFeatsCrossValidResults.CAIMDisc.rpart.k10,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.rda")
save(rankedFeatsCrossValidResults.CAIMDisc.nb.k10,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.nb.k10.rda")
save(rankedFeatsCrossValidResults.CAIMDisc.svm.k10,
     file="./objs/rankedFeatsCrossValidResults.CAIMDisc.svm.k10.rda")

load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.rpart.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.nb.k10.rda")
load(file="./objs/rankedFeatsCrossValidResults.CAIMDisc.svm.k10.rda")




#########################################################################
#############
#   Shrunken centroids gene expression filter...
#        a similar method to that used in Mammaprint...

require(pamr)
require(survival)
class.attr <- colnames(expMatLRtWithClassInfo)[ncol(expMatLRtWithClassInfo)]
classLabel <- expMatLRtWithClassInfo[,class.attr]
classLabel <- gsub("1","high",gsub("2","medium",gsub("3","low",as.character(classLabel))))
summary(as.factor(classLabel))
#  high    low medium 
#   708    735    205 
expMatLRt <- t(expMatLRtWithClassInfo[,colnames(expMatLRtWithClassInfo)!=class.attr])
dim(expMatLRt)
# [1] 13752  1648
table(classLabel)
# classLabel
#   high    low medium 
#    708    735    205 

sdevs <- apply(expMatLRt,1,sd)
expMatLRt <- expMatLRt[sdevs > median(sdevs),]
dim(expMatLRt)
# [1] 6875 1648
sampleid <- colnames(expMatLRt)
geneid <- rownames(expMatLRt)
genenames <- rownames(expMatLRt) 
train.dat <- list(x=expMatLRt,y=classLabel,genenames=genenames,geneid=geneid,sampleid=sampleid)
model <- pamr.train(train.dat, n.threshold=100)
model

model.cv <- pamr.cv(model,train.dat,nfold=10)
model.cv
tiff(filename="./figs/expMatLRtShrunkenCentroid10FoldCV.performance.tiff",
     height=3000,width=3000,res=350)
pamr.plotcv(model.cv)
dev.off()

# to investigate if the large  error-rates observed are  due to a three class problem...
#    - modifying the class labels into two classes...
classLabel <- expMatLRtWithClassInfo[,class.attr]
classLabel <- gsub("1","high",gsub("2","high",gsub("3","low",as.character(classLabel))))
table(classLabel)
#classLabel
#high  low 
# 913  735 
train.dat <- list(x=expMatLRt,y=classLabel,genenames=genenames,geneid=geneid,sampleid=sampleid)
model <- pamr.train(train.dat, n.threshold=100)
model
model.cv <- pamr.cv(model,train.dat,nfold=10)
tiff(filename="./figs/expMatLRtShrunkenCentroid10FoldCVHighvsLow.performance.tiff",
     height=3000,width=3000,res=350)
pamr.plotcv(model.cv)
dev.off()
model.cv

#
pamr.confusion(model.cv,threshold=2.3,extra=FALSE)
#       predicted
# true   high low
#   high  573 343
#   low   347 388
pamr.confusion(model.cv,threshold=2.3,extra=TRUE)
#      high low Class Error rate
# high  568 345        0.3756846
# low   361 374        0.4721088
# Overall error rate= 0.418

tiff(filename="./figs/expMatLRtShrunkenCentroidPlots.tiff",
     height=3000,width=3000,res=350)
pamr.plotcen(model,train.dat,2.3)
dev.off()

tiff(filename="./figs/expMatLRtShrunkenCentroidCVProbPlots.tiff",
     height=3000,width=3000,res=350)
pamr.plotcvprob(model,train.dat,2.3)
dev.off()


# NOTE: the classifier inevitably makes incorrect predictions, both false positives and false negatives, for some
#  patients. Selecting the optimal number of genes involves making a trade off between these two errors. To
#  visually inspect the prognostic quality of the classifier, we separate the patients based on the predicted
#  prognosis and plot Kaplan-Meir curves for Both....

Delta <- 2.3 # from visualizing the cv plot


### ---- evaluating predictive/prognostic quality ---------------
# predictions <- model.cv$yhat[,which(model.cv$threshold>Delta)[1]]
# plot(survfit(Surv(known.patients$t.dmfs, known.patients$e.dmfs) ~ predictions),
#      col=c(1,2),xlab="Time (years)",ylab="Distant metastasis free survival")
# legend("bottomright",legend=levels(known.patients$prognosis),fill=1:nlevels(known.patints$prognosis))
### -------------------------------------------------------------

load("./objs/allMergedNoDupsExpressionSetEBayesAdjusted.rda")
expMatLRtESet <- allMergedNoDupsExpressionSetEBayesAdjusted[,colnames(expMatLRt)]
expMatLRtPData <- pData(expMatLRtESet)
lr.time <- as.numeric(expMatLRtPData$lr.time)
lr.event <- rep(1,length(lr.time))
lr.event[which(lr.time > 60)] <- 0
predictions <- model.cv$yhat[,which(model.cv$threshold>Delta)[1]]


png(filename="./figs/expMatLRtShrunkenCentroid5yKMPlot.png",
      height=1500,width=1500,res=300)
plot(survfit(Surv(lr.time, lr.event) ~ predictions),
      col=c(1,2),xlab="Time (months)",ylab="Recurrence free survival",las=1)
legend("bottomright",legend=levels(as.factor(classLabel)),fill=1:nlevels(as.factor(classLabel)))
dev.off()


pamr.geneplot(model, train.dat, 3.14)# Plots the raw gene expression for genes that
                                         #  survive the specified threshold. 
 # should you get the error message "Error in plot.new(): Figure margins too large", increase the delta.

selectedFeatsShrunkenCentroid <- pamr.listgenes(model,train.dat,Delta,genenames=TRUE)
save(selectedFeatsShrunkenCentroid, file="./objs/selectedFeatsShrunkenCentroid.rda")
load(file="./objs/selectedFeatsShrunkenCentroid.rda")

# ---- for computaitonal Diagnosis (or independent validation) ---
# pamr.predict(model,exprs(new.patients),Delta)
# pamr.predict(model,exprs(new.patients),type="posterior")
# the 'posterior' type parameter/argument tells PAM to also
#      report the posterior class probabilities, that is, how sure PAM is about
                                           # the predictions...
