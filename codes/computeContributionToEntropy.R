# computeContributionToEntropy.R
# Paul Aiyetan
# May 25, 2016



# the method implements Alter et al (2000) and Varshasvsky et al (2006) entropy
#      contribution....This is given by the equations...
#
#         V_j = ((s_j)^2)/((s_1)^2 + (s_2)^2 + (s_3)^2 ... + (s_L)^2) ........ [1]
#
#            where V_j is the relative significance of the jth eigengene and eigenarray in terms of the
#               fracton of the overall expression that they capture...
#
#        "Shanon entropy' of the dataset 'E' is

#         E = - (1/log(L)) * ((V_1*(log(V_1))) + (V_2*(log(V_2))) + (V_3*(log(V_3)))...+ (V_L*(log(V_L))))



computeContributionToEntropy <-
    function(expMat, method = "svd"){
        require("parallel")
        source("./codes/computeEntropy.R")
        cl <- makeCluster(getOption("cl.cores",7))
        features <- rownames(expMat)
        featureCEList <- # each features contribution to entropy...
            parLapply(cl, features, function(x){
               source("./codes/computeEntropy.R")
               # for each feature,
               # CE = E_(initial.matrix entropy) - E_(initial.matrix without feature entropy)
               # print(paste("....computing contribution to entropy of feature ", x, sep=""))
               e.init.mat <- computeEntropy(expMat)
               e.later.mat <- computeEntropy(expMat[rownames(expMat)!= x,])
               return(e.init.mat - e.later.mat)  
            })
        stopCluster(cl)
        names(featureCEList) <- features
        return(featureCEList)
    }

