# createWekaARFFInputFile.R
# Paul Aiyetan
# July 11, 2016



# This function takes as an input a class-information-containing data object, typically a data.frame
#  or a matrix and creates a 'Weka' ARFF input file format output.


createWekaARFFInputFile <-
    function(objWithClassInfo, outputFilePath){
      
      # the ARFF (Attribute-Relation File Format) is an ASCII test file that describes a 
      #  list of instances  sharing a set of attributes. It consits of two sections,
      #    1) the header section (personnal sub-divided into:
      #       a) preamble 
      #       b) @relation definition
      #       c) @attribute  definition
      #    2)@ data definition 
      
      pb <- txtProgressBar(min=0, max=100,initial=0,style=3)
      # write header...
      # write preamble...
      write(paste("% ",basename(outputFilePath),
                  "\n% Author: Paul Aiyetan, MD \n% Date: ",date(),"\n\n",collapse="", sep=""),file=outputFilePath)  
      setTxtProgressBar(pb,3)            
      
      # @relation
      write(paste("@RELATION ",gsub(".arff","",basename(outputFilePath)),collapse="", "\n",sep=""),
            file=outputFilePath,append=TRUE)
      setTxtProgressBar(pb,5)  
      
      # @attribute
      fileAttributes <- colnames(objWithClassInfo)[-ncol(objWithClassInfo)] 
           # get all columnss except the class label column - which is by default expected in the last column
      for(i in 1:length(fileAttributes)){
	write(paste("@ATTRIBUTE ", fileAttributes[i], " NUMERIC",collapse="",sep=""),
	      file=outputFilePath, append=TRUE)      
      }
      # get unique values of class labels 
      classLabel <- colnames(objWithClassInfo)[ncol(objWithClassInfo)] 
      classLabelValuesUnique <- unique(objWithClassInfo[,classLabel])
      write(paste("@ATTRIBUTE ", 
                  classLabel," ", 
                  paste("{",
                        paste(classLabelValuesUnique,collapse=",",sep=""),
                        "}",sep="",collapse=""),
                  "\n",collapse="",sep=""),
	      file=outputFilePath, append=TRUE)
      setTxtProgressBar(pb,10) 
      
      # @data
      write("@DATA",file=outputFilePath,append=TRUE)
      #classLabelValues <- objWithClassInfo[,classLabel]
      for(i in 1:nrow(objWithClassInfo)){
	  write(paste(objWithClassInfo[i,],collapse=",",sep=""),file=outputFilePath,append=TRUE)
	  setTxtProgressBar(pb,10 + ((i/nrow(objWithClassInfo)) * 90))   
      }  
      return(TRUE)
    }

