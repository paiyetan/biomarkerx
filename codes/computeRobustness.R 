# computeRobustness.R (stability)
# Paul Aiyetan
# 09/08/2016


computeRobustness <- 
  function(robustnessEstimates){
    
    robustness <- list()
    filterMethods <- names(robustnessEstimates)    
    for(i in 1:length(filterMethods)){    
      filterMethod <- filterMethods[i]
      filterMethodEvalAlgorithmsList <- robustnessEstimates[[filterMethod]]

      evaluationAlgorithms <- names(filterMethodsEvalAlgorithmsList)
      for(j in 1:length(evaluationAlgorithms)){
	eval.algorithm <- evaluationAlgotrithms[j]	
	filterMethodEvalAlgorithmSubSamplesList <- filterMethodEvalAlgorithmsList[[eval.algorithm]]
	
	# get the search methods (algorithms)
	search.algorithms <- names(filterMethodEvalAlgorithmSubSamplesList[[1]])
	# for each search.algorithm get the selected features in the respective subsample 
	for(k in 1:length(search.algorithms)){
	  search.algorithm <- search.algorithms[k]
	  selectedFeaturesList <- list()
	  selectedFeaturesList <- lapply(filterMethodEvalAlgorithmSubSamplesList,
				      function(x){
				         return(x[[search.algorithm]][["selectedFeatures"]])
				      })
	  # computeRobustness on the selecctedFeatures 
	  robustness[[paste(filterMethod, ".",
	                    eval.algorithm, ".",
	                    search.algorithm,
	                    sep="",collapse="") ]] <- estimateRobustness(selectedFeaturesList) 				
	  
	}
      
      }

  }
  
estimateRobustness <- 
  function(selectedFeaturesList){
  # by default, this implementation (following Kalousis et al (2007)) employs the Jaccard index(score) as a similarity
  #  measure to estimate robustness as decribed in Saeys Y et al 2008. other measures include the Kuncheva index (KI) -
  #  initally described by 
  #        Kuncheva (2007). The Kuncheva Index was subsequently implemented in a later publication from Saeys' group in 
  #        Abeel T et al. (2010). However, it appears the KI assumes equal length for features to be compared. For feature
  #         subset returned from a wrapper approach employing our search methods, the chances that the number of optimal
  #         features returned for each pertubation cannot be gauranteed to be of the same length
  
  #  S_tot = [ 2 * Summation_i=1,^k Summation^k_j=i+1 S(f_i, f_j) ] / k(k-1) 
  
  k <- length(selectedFeaturesList)
  sumMatrix <- matrix(0, nrow=k, ncol=k)
  for(i in 1:(nrow(sumMatrix)-1)){
    for(j in (i+1):ncol(sumMatrix)){
      featuresA <- selectedFeaturesList[[i]]
      featuresB <- selectedFeaturesList[[j]]
      sumMatrix[i,j] <- length(intersect(featuresA, featuresB))/length(union(featuresA, featuresB)) #jaccard index
    }  
  }
  
  S_tot <- sum(sumMatrix)/ (k * (k-1))
  return(S_tot)
  
}
