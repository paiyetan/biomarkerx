# combinePhenoData.R
# Paul Aiyetan
# May 19, 2016




# similar to the combineExpressionMatrices function, this function takes a list of ExpressionSets, combining
#   the phenoData

combinePhenoData <-
    function(esetsList){
        pDataList <- lapply(esetsList, function(x) return(pData(x)))
        pDataVariates <- lapply(pDataList,function(x) return(colnames(x))) # extract the covariates..
        # extract the common covariates...
        commonVariates <- pDataVariates[[1]]
        for(i in 2:length(pDataVariates)){
            commonVariates <- intersect(commonVariates,pDataVariates[[i]])
        }
        # subset from each expression matrix the common
        combinedPData <- NULL
        if(length(commonVariates)!= 0){
            pDataList <- lapply(pDataList, function(x) return(x[,commonVariates]))
            combinedPData <- do.call("rbind",pDataList)
        }else{
            stop("no columns in common in pData...")
        }
        return(combinedPData)
   }
