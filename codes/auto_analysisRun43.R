# auto_analysisRun43.R
# Paul Aiyetan
# 2016/10/21


# in this analysis run, we evaluate the previous built (predictive) model
#   on an independent datasets (Planey et al)...

rm(list=ls())
require("Biobase")
require("rpart")
require("e1071")
require("FSelector")

load(file="./objs/lr.fSMtdsModels.rda")
load(file="./objs/lr.peakPerfFeats.rda")
load(file="./objs/inDataExpMatDFStWithBiClassInfo.rda")
source("./codes/runIndependentValidations.R")


lr.fSMtdsIds <- names(lr.fSMtdsModels)
#lr.fSMtdsModelsIndValidatons <-
#    lapply(lr.fSMtdsIds,function(x, j=lr.fSMtdsModels,k=lr.peakPerfFeats,
#                              z=inDataExpMatDFStWithBiClassInfo){

lr.fSMtdsModelsIndValidatons <- list()
for(i in 1:length(lr.fSMtdsIds)){
    x <- lr.fSMtdsIds[i]
    j <- lr.fSMtdsModels
    k <- lr.peakPerfFeats
    z <- inDataExpMatDFStWithBiClassInfo
        
    modeL <- j[[x]][["model"]] # get the previously derived  learning model...

    fSMtdFeatures <- k[[x]]
    dataMat <- z[,c(fSMtdFeatures,colnames(z)[ncol(z)])]
    class.attr <- colnames(dataMat)[ncol(dataMat)]

   dataMat <- as.data.frame(dataMat)
    dataMat[,class.attr] <- as.factor(dataMat[,class.attr])

    test <- dataMat
    #----exclude questionable dfs values----#
    #test <- test[c(109:nrow(test)),]

    true.value <- test[, class.attr]
    prediction.value <- predict(modeL,test)#, probability = TRUE)
    if(class(prediction.value)== "matrix"){
        preds <- c()
        for(m in 1:nrow(prediction.value)){
            rowValue <- ifelse(prediction.value[m,"0"] > prediction.value[m,"1"], 0, 1)
            preds <- c(preds,rowValue)
        }
        prediction.value <- as.factor(preds)
    }
    
    #error.rate <- sum(true.value != prediction.value)/nrow(test)

    xt <- table(prediction.value,true.value) 
    pred.perfs <- runPerfMeasures(xt)

    #return(pred.perfs)
    lr.fSMtdsModelsIndValidatons[[x]] <- pred.perfs
}
 
lr.fSMtdsModelsIndValidatons <- do.call("rbind",lr.fSMtdsModelsIndValidatons)
save(lr.fSMtdsModelsIndValidatons,file="./objs/lr.fSMtdsModelsIndValidatons.rda")
load(file="./objs/lr.fSMtdsModelsIndValidatons.rda")
