# selectFeatures.R
# Paul Aiyetan
# May 23, 2016





# this function acts as a wrapper for a subset of unsupervised feature selection method
# in this wrapper methods, three major approaches are currently implemented.
# These include:
#   1) maximum variance (max.var)
#   2) remove redundancy + maximum variance (r.max.var), and
#   3) contribution to entropy (CE) Varshavsky et al (2006)
#       i) simple ranking according to entropy (ce.sr)
#      ii) forward selection according to highest entropy (ce.fs1)
#     iii) forward selection by best CE out of the remaining (ce.fs2)
#      iv) backward elimination of features with lowest ce (ce.be)
#   4) maximum range (max.range)


#  Other methods:
#   - optimal correlation with the first principal component



selectFeatures <-
    function(data,
             method, # "max.var", "max.range", "r.max.var", "ce.sr", "ce.fs1", "ce.fs2", "ce.be"
             noOfFeatures){

      if(method=="max.var"){ # Herrero, 2003; Guyon and Elissef, 2003
          # maximally varying features....
          rowNames <- rownames(data)
          data.var <- apply(data,1,var, na.rm = TRUE)
          data.var.table <- cbind(data.var,as.data.frame(data),stringsAsFactors=FALSE)
          rownames(data.var.table) <- rowNames
          data.var.table.ordered <- data.var.table[order(data.var.table$data.var, decreasing=TRUE),]
                       # ordered in decreasing variance....
          if(missing(noOfFeatures)){
              # default subset top 150 most variable features....
              selectedFeatures <- rownames(data.var.table.ordered)[1:150]
          }else{
              selectedFeatures <- rownames(data.var.table.ordered)[1:noOfFeatures]
          }

          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures
                      )
                 )
      }

      if(method=="max.range"){ # Herrero, 2003; Guyon and Elissef, 2003
          # features with maximal range....
          data.var <- apply(data,1,function(x){
                            rrange <- range(x, na.rm=TRUE)
                            return(rrange[2] - rrange[1])})
          data.var.table <- cbind(data.var,as.data.frame(data),stringsAsFactors=FALSE)
          data.var.table.ordered <- data.var.table[order(data.var.table$data.var, decreasing=TRUE),]
                       # ordered in decreasing variance....
          if(missing(noOfFeatures)){
              # default subset top 150 most variable features....
              selectedFeatures <- rownames(data.var.table.ordered)[1:150]
          }else{
              selectedFeatures <- rownames(data.var.table.ordered)[1:noOfFeatures]
          }

          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures
                      )
                 )
      }

      if(method=="r.max.var"){
          # remove redundancy and return the most maximally varying features...
          corMat <- matrix(nrow=nrow(data),ncol=nrow(data),
                           dimnames=list(rownames(data),rownames(data)))
          # populate correlation matrix and pairWiseCorr list object for easy access to these
          pairWiseCorr <- list()
          pairWiseCorrIndex <- 1
          for(i in 1:nrow(corMat)){
              for(j in 1:ncol(corMat)){
                  if(j > i){
                      featureA <- rownames(corMat)[i]
                      featureB <- colnames(corMat)[j]
                      featAExp <- data[featureA,]
                      featBExp <- data[featureB,]
                      correlation <- cor(x = featAExp, y = featBExp,
                                         method = "pearson")
                      corMat[i,j] <- correlation
                      pairWiseCorr[[pairWiseCorrIndex]] <-
                          list(featureA=featureA,featureB=featureB,
                               correlation=correlation)
                      pairWiseCorrIndex <- pairWiseCorrIndex + 1
                  }
              }
          }

          availableFeatures <- rownames(data)
          # resolve redundant highly correlated features...
          for(i in 1:length(pairWiseCorr)){
              correlation <- pairWiseCorr[[i]]$correlation
              if(correlation > 0.99){
                 # retain feature with the higher variance....
                 featureA <- pairWiseCorr[[i]]$featureA
                 featureB <- pairWiseCorr[[i]]$featureB
                 if(featureA %in% availableFeatures &
                    featureB %in% availableFeatures){
                      featAExp <- data[featureA,]
                      featBExp <- data[featureB,]
                      varA <- var(featAExp,na.rm=TRUE)
                      varB <- var(featBExp,na.rm=TRUE)
                      if(varA >= B){
                          availableFeatures <-
                              availableFeatures[availableFeatures != featureB]
                      }else{
                         availableFeatures <-
                              availableFeatures[availableFeatures != featureA]
                     }
                 }
              }
          }
          # subset remaining non-highly correlated features
          data <- data[availableFeatures,]
          rowNames <- rownames(data)
          # subset maximally varying
          data.var <- apply(data,1,var, na.rm = TRUE)
          data.var.table <- cbind(data.var,as.data.frame(data),stringsAsFactors=FALSE)
          rownames(data.var.table) <- rowNames
          data.var.table.ordered <- data.var.table[rev(order(data.var.table$data.var)),]
                       # ordered in decreasing variance....
          if(missing(noOfFeatures)){
              # default subset top 150 most variable features....
              selectedFeatures <- rownames(data.var.table.ordered)[1:150]
          }else{
              selectedFeatures <- rownames(data.var.table.ordered)[1:noOfFeatures]
          }

          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures,
                      corMat=corMat,
                      pairWiseCorr=pairWiseCorr
                      )
                 )
      }

      if(method=="ce.sr"){
          # ensure compute entropy function is loaded....
          source("./codes/computeContributionToEntropy.R")
          source("./codes/computeEntropy.R")
          source("./codes/getFeaturesToCEMapTable.R")
          featureToEntropyContributionMap <-
              computeContributionToEntropy(data,method="svd")
          map.table <- getFeaturesToCEMapTable(featureToEntropyContributionMap,
                                               ordered=TRUE)
          selectedFeatures <- map.table$features[1:noOfFeatures]
          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures,
                      featureToEntropyContributionMap=featureToEntropyContributionMap)
                 )
      }

      if(method=="ce.fs1"){
          # according to Varshavsky et al..
          #    1) first, choose the feature with the highest CE,
          #    2) from the remaining feature set, choose the feature that together with the previouly
          #       selected feature(s) produces the highest entropy.
          #    3) select the best feature(s) combination..
          #    4) repeat steps 2 and 3 until the total number of desired features area selected
          #
          source("./codes/computeContributionToEntropy.R")
          source("./codes/computeEntropy.R")
          source("./codes/getFeaturesToCEMapTable.R")

          #require(foreach)
          require(parallel)

          # instantiate feature list vector...
          selectedFeatures <- c()
          # choose the first feature...
          featureToEntropyContributionMap <-
              computeContributionToEntropy(data,method="svd")
          mapTable <- getFeaturesToCEMapTable(featureToEntropyContributionMap,
                                               ordered=TRUE)
          feature1 <- mapTable$features[1]# as a first step, select the feature with the highest CE
          selectedFeatures <- c(selectedFeatures, feature1)
          #t3 <- proc.time()
          while(length(selectedFeatures) < noOfFeatures){
              # subset the already selected features from the data matrix...
              subData <- data[!(rownames(data) %in% c(selectedFeatures)),]
              subFeatures <- rownames(subData)
              entropies <- as.numeric(rep(NA,length(subFeatures)))
              #for(i in 1:length(subFeatures)){
              ## using the 'foreach' parallel programming paradigm....
              #X <- foreach(i = 1:length(subFeatures)) %dopar% {
              #    subFeature <- subFeatures[i]
              #    selectedFeaturesWithSubFeature <- c(selectedFeatures,subFeature)
              #    selectedFeaturesWithSubFeatureDataTable <- data[selectedFeaturesWithSubFeature,]
              #    # entropies[i] <-
              #    computeEntropy(selectedFeaturesWithSubFeatureDataTable)
              #}
             # entropies <- unlist(X)

<<<<<<< HEAD
              # using the 'parallel' r-pkg parallel programing paradigm...
              cl <- makeCluster(getOption("cl.cores",3))
=======
              # using the 'parallel' r-pkg parallel programing paradigm... 
              cl <- makeCluster(getOption("cl.cores",7))
>>>>>>> 61de72267e7c601a28d3f0fbc301df9357fefd38
              clusterExport(cl, varlist=c("selectedFeatures","data"),envir=environment())
              X <- parLapply(cl,subFeatures,function(x,y=selectedFeatures){
                        source("./codes/computeEntropy.R")
                        selectedFeaturesWithSubFeature <- c(y,x)
                        selectedFeaturesWithSubFeatureDataTable <- data[selectedFeaturesWithSubFeature,]
                        entropy <- computeEntropy(selectedFeaturesWithSubFeatureDataTable)
                        return(entropy)
                     })
              entropies <- unlist(X)
              stopCluster(cl)

              subFeatureCETable <- data.frame(subFeatures,entropies,stringsAsFactors=FALSE)
                  #  a collection object for derived entropies associated with
                  #  the addition of the respective sub feature(s) with the previously selected
                  #  features...
              subFeatureCETable <- subFeatureCETable[order(subFeatureCETable$entropies,decreasing=TRUE),]
              entropyMaximizingFeature <- subFeatureCETable$subFeatures[1]
              selectedFeatures <- c(selectedFeatures,entropyMaximizingFeature)
          }
          #t4 <- proc.time()
          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures # entropy maximizing features...
                      )
                 )

          # NOTES: Processing times
          # t2-t1 (using the 'foreach' implementation
          #     user   system  elapsed
          # 17260.38     0.00 17252.67
          # approx: 4.792 hours, with a warning message:
          # Warning message:
          # executing %dopar% sequentially: no parallel backend registered

          # a second attempt, with possibly parallel backend register
          # t4-t3
          # user   system  elapsed
          # 9765.256  120.528 9830.923
          # approx: 2.7308 hours

          #
          # t4 - t3 (using the 'parallel' implementation...
          #    user   system  elapsed
          #  39.448   30.460 1855.446
          # approx: .5154 hours (about 30 mins)
      }

      if(method=="ce.fs2"){
          # according to Varshavsky et al..
          #    1) first, choose the feature with the highest CE,
          #    2) recompute CE with the remaining features, and select the feature with the highest CE
          #    3) repeat steps 2 until the total number of desired features are selected
          #
          source("./codes/computeContributionToEntropy.R")
          source("./codes/computeEntropy.R")
          source("./codes/getFeaturesToCEMapTable.R")

          #require(foreach)
          require(parallel)

          # instantiate feature list vector...
          selectedFeatures <- c()
          # choose the first feature...
          featureToEntropyContributionMap <-
              computeContributionToEntropy(data,method="svd")
          mapTable <- getFeaturesToCEMapTable(featureToEntropyContributionMap,
                                               ordered=TRUE)
          feature1 <- mapTable$features[1]# as a first step, select the feature with the highest CE
          selectedFeatures <- c(selectedFeatures, feature1)
          #t1 <- proc.time()
          while(length(selectedFeatures) < noOfFeatures){
              # subset the already selected features from the data matrix...
              subData <- data[!(rownames(data) %in% c(selectedFeatures)),]
              featureToEntropyContributionMap <- computeContributionToEntropy(subData,method="svd")
              mapTable <- getFeaturesToCEMapTable(featureToEntropyContributionMap,
                                                  ordered=TRUE)
              topFeature <- mapTable$features[1] # as a first step, select the feature with the highest CE
              selectedFeatures <- c(selectedFeatures, topFeature)

          }
          #t2 <- proc.time()

          return(list(subData=data[selectedFeatures,],
                      selectedFeatures=selectedFeatures # entropy maximizing features...
                      )
                 )

          # NOTES: Processing times
          # t2 - t1
          #   user     system    elapsed
          # 15.832      4.404 208053.030
          # approx. 57.7925 hours (~ 2 days, 10 hours)
      }

      if(method=="ce.be"){
          # according to Varshavsky et al..
          #    1) first, choose the feature with the lowest CE and remove from the total number of features..
          #    2) recompute CE with the remaining features,
          #    3) repeat steps 2 until the number of remaining features equals number of features desired
          #
          source("./codes/computeContributionToEntropy.R")
          source("./codes/computeEntropy.R")
          source("./codes/getFeaturesToCEMapTable.R")

          #require(foreach)
          require(parallel)

          t3 <- proc.time()
          # choose the first feature...
<<<<<<< HEAD
          subData <- data
          while(nrow(subData) < noOfFeatures){
=======
          subData <- data          
          while(nrow(subData) >= noOfFeatures){
>>>>>>> 61de72267e7c601a28d3f0fbc301df9357fefd38
              featureToEntropyContributionMap <- computeContributionToEntropy(data,method="svd")
              mapTable <- getFeaturesToCEMapTable(featureToEntropyContributionMap,
                                              ordered = TRUE, decreasing = FALSE)
              leastFeature <- mapTable$features[1] # as a first step, select the feature with the lowest CE
              subData <- data[!(rownames(data) %in% c(leastFeature)),]

          }
          t4 <- proc.time()

          return(list(subData=subData,
                      selectedFeatures=rownames(subData)
                      )
                 )

      }

      #if(method=="gene.shaving"){
      #
      #
      #}

    }







































































































































