# chiM.parallel.R
# Paul Aiyetan
# July 08, 2016

# 

chiM.parallel <- 
  function (data, alpha = 0.05) 
  {
    require("foreach")
    require("discretization")
    require("doParallel")
    
    require("parallel")
    
    p <- dim(data)[2] #the number of columns in the data matrix/data.frame...
    discredata <- data
    #cutp <- list()
    #for (i in 1:(p - 1)) {
    
    cl <- makeCluster(1)
    registerDoParallel(cl)
    cutp <- foreach(i=1:(p - 1), .export=c("discredata","alpha","data")) %dopar% {
      require(discretization)
      val <- value(i, data, alpha)
      #cutp[[i]] <- val$cuts
      discredata[, i] <- val$disc[, i]
      return(val$cuts)
    }
    
    p <- dim(data)[2] #
    discredata <- data
    cutp <- list()
    
    
    for (i in 1:(p - 1)) {
      val <- value(i, data, alpha)
      cutp[[i]] <- val$cuts
      discredata[, i] <- val$disc[, i]
    }
    
    
    
    
    return(list(cutp = cutp, Disc.data = discredata))
    }
    
    
    
    stopCluster(cl)
    
    return(list(cutp = cutp, Disc.data = discredata))
  }
