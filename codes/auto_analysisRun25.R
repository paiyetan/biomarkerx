# auto_analysisRun25.R
# Paul Aiyetan
# 09/22/2016



# this analysis run runs implements a logistic regression (+/- a cox proportional
#   hazard function on surival) on the repective samples using the selected features for
#   previously identified optimal performing feature selection peaks...



# refs:
# https://www.r-bloggers.com/how-to-perform-a-logistic-regression-in-r/
# http://www.ats.ucla.edu/stat/mult_pkg/faq/general/odds_ratio.htm
# http://www.ats.ucla.edu/stat/r/dae/logit.htm
# http://nlp.stanford.edu/manning/courses/ling289/logistic.pdf
# https://ww2.coastal.edu/kingw/statistics/R-tutorials/logistic.html



rm(list=ls())
require(survival)
require(FSelector)
require(Biobase)
source("./codes/computeFeaturesModels.R")


#########################################
# expMatLRtWithBiClassInfo
load("./objs/expMatLRtWithBiClassInfo.topFeatsSpecResultsTableFeatures.rda")
load("./objs/expMatLRtWithBiClassInfo.rda")
load("./objs/allMergedNoDupsExpressionSetEBayesAdjusted.rda")
# read-in peak accuracy feature selection table file.....
topFeatsPeaks <- read.table("./texts/expMatLRtWithBiClassInfo.topFeatsPeaks.tsv",header=TRUE,sep="\t",
                            stringsAsFactor=FALSE)

# downstream analyses...
figs.out <- "./figs/expMatLRtWithBiClassInfo.logit.models"
txts.out <- "./texts/expMatLRtWithBiClassInfo.logit.models"
dir.create(figs.out, showWarnings=FALSE)
dir.create(txts.out, showWarnings=FALSE)

expMatLRtWithBiClassInfo.logit.models <-
    computeFeaturesModels(selFeatsTable = expMatLRtWithBiClassInfo.topFeatsSpecResultsTableFeatures,
                            expMatWithClassInfo = expMatLRtWithBiClassInfo,
                              topFeatsPeaks = topFeatsPeaks,
                                method = "logit",
                                  figs.out = figs.out,
                                    txts.out = txts.out)

save(expMatLRtWithBiClassInfo.logit.models,
     file="./objs/expMatLRtWithBiClassInfo.logit.models.rda")
load(file="./objs/expMatLRtWithBiClassInfo.logit.models.rda")



# using coxph model...
  expMat <- exprs(allMergedNoDupsExpressionSetEBayesAdjusted)
  pheno <- pData(allMergedNoDupsExpressionSetEBayesAdjusted)
  lr.time <- as.numeric(pheno[,"lr.time"])
  samples.without.nas <- colnames(expMat)[!is.na(lr.time)]
  expMatLR <- expMat[,samples.without.nas]
  lr.time <- as.numeric(pheno[samples.without.nas,"lr.time"]) 
  expMatLRtransposed <- t(expMatLR)
  expMatLRtWithClassInfo <- cbind(expMatLRtransposed,lr.time)
# save class-info-contatining table object
save(expMatLRtWithClassInfo, file="./objs/expMatLRtWithClassInfo.rda")
load(file="./objs/expMatLRtWithClassInfo.rda")

figs.out <- "./figs/expMatLRtWithBiClassInfo.coxph.models"
txts.out <- "./texts/expMatLRtWithBiClassInfo.coxph.models"
dir.create(figs.out, showWarnings=FALSE)
dir.create(txts.out, showWarnings=FALSE)

expMatLRtWithBiClassInfo.coxph.models <-
    computeFeaturesModels(selFeatsTable = expMatLRtWithBiClassInfo.topFeatsSpecResultsTableFeatures,
                            expMatWithClassInfo = expMatLRtWithClassInfo, #use expMat with actual values..
                              topFeatsPeaks = topFeatsPeaks,
                                method = "coxph",
                                  figs.out = figs.out,
                                    txts.out = txts.out)

save(expMatLRtWithBiClassInfo.coxph.models,
     file="./objs/expMatLRtWithBiClassInfo.coxph.models.rda")
load(file="./objs/expMatLRtWithBiClassInfo.coxph.models.rda")






